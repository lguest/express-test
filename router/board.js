var express = require('express');
var router = express.Router();
//var sqlite3 = require('sqlite3').verbose();
//var db = new sqlite3.Database(':memory:');

//컨트롤러 로드
var boardController = require('../controller/board.ctrl');

router.get('/', boardController.index); //초기화면

router.get('/list', boardController.list); //리스트

router.get('/view', boardController.view); //뷰

router.get('/write', boardController.write); //작성폼

router.post('/proc_write', boardController.proc_write); //작성처리 

module.exports = router;