var express = require('express');
var router = express.Router();
// respond with "hello world" when a GET request is made to the homepage
const controller = require('../controller/test.ctrl');

router.get('/page', controller.test);
router.get('/page2', controller.test2);


module.exports = router;