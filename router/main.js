var express = require('express');
var router = express.Router();
// respond with "hello world" when a GET request is made to the homepage

router.get('/', function (req, res) {
    //로그인 세션 처리 
    if (req.session.user_id == null){ //세션이 없을경우 로그인 페이지로 보낸다.
         res.redirect(301, '/member/login');
    }else{
        

        res.render('index', {
            title: "MY HOMEPAGE",
            length: 5,
            session:req.session
        });
    }
});

module.exports = router;