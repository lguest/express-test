module.exports = function (connection) { //모듈을 익명함수로 만들어 app에서 생성한 connection 을 전달 받았다.
    var express = require('express');
    var router = express.Router();
    // respond with "hello world" when a GET request is made to the homepage
    var common = require('../modules/common');    

    router.get('/login', function (req, res) {
        res.render('member/login', {
            title: "로그인 페이지"
        });
    });

    router.post('/login', function (req, res) {

        var user_id = req.body.user_id;
        var user_pwd = req.body.user_pwd;

        console.log(req.body);
        if (user_id == '' || user_pwd == '') {
            res.send(common.goBack('아이디 및 비밀번호를 입력하십시오. '));            
            //res.redirect(req.header('Referer')); //이전페이지로 이동
        }else{
        
            //DB 접속 후 회원정보 로드         
            var query  = connection.query('SELECT * FROM user WHERE user_id = ?', [user_id], function(error, result, field){
                if (error){ //쿼리 에러시 종료
                    console.log(error.code);
                    res.status(500).end();
                }
                console.log(result);
                numRows = result.length;
                if(user_pwd == result[0].user_pwd){//로그인 성공시 세션 생성
                    req.session.user_id = user_id;
                    req.session.user_name = result[0].user_name;

                    res.redirect('/'); //메인페이지로 이동 
                }else{
                    res.send(common.goBack('아이디 및 비밀번호를 확인 하십시오. '));                    
                }                
            });              
        }
    });

    router.get('/logout', function (req, res){
        //req.session.destory();  // 세션 삭제
        req.session = null;  // 세션 삭제
        req.redirect('/member/login');
    })

    return router;
}

//module.exports = router;