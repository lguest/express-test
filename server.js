var http = require('http');
var express = require('express');
var fs = require('fs');
var app = express();
var url = require('url');

var server = http.createServer(app);

app.set('views', __dirname + '/views'); //뷰디렉토리 설정
app.set('view engine', 'ejs'); //html 렌더링시 ejs 모듈을 사용 (jade 도 있다- 문법이 다름 뷰템플릿)
app.engine('html', require('ejs').renderFile);
app.use(express.static('public')); //정작파일 폴더 설정

var bodyParser = require('body-parser'); //모듈로드
app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
})); 

app.get('/',function(req,res){
    
    res.render('./index', {
        title:"인덱스페이지",length:5
    });
});

console.log('server start');
server.listen(4000);