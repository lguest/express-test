var express = require('express'); //express 모듈로드
var app = express();                   //생성
var fs = require('fs');
var session = require('express-session'); //세션 모듈 로드 
var cookieParser = require('cookie-parser'); //쿠키 파서 모듈
var common = require('./modules/common'); //공통 함수 
//세션체크 미들웨어
var sessionCheck = require('./middleWare/session.check');

app.use(session({    //기본 세션설정
    secret: '@#@$MYSIGN#@$#$',
    resave: false,
    saveUninitialized: true
}));

app.use(cookieParser()); //쿠키파서 사용

var mysql = require('mysql'); //mysql 모듈 로드 
var database = require('./config/database'); //데이터베이스 설정파일

var connection = mysql.createConnection(database);

app.set('views', __dirname + '/views'); //뷰디렉토리 설정
app.set('view engine', 'ejs'); //html 렌더링시 ejs 모듈을 사용 (jade 도 있다- 문법이 다름 뷰템플릿)
app.engine('html', require('ejs').renderFile);
app.use(express.static('public')); //정작파일 폴더 설정
/*
var myLogger = function (req, res, next) {
    console.log('LOGGED');
    next();
};

var requestTime = function (req, res, next) {
    req.requestTime = Date.now();
    next();
};

//미들웨어 테스트 
app.use(myLogger);
app.use(requestTime);
*/
var bodyParser = require('body-parser'); //모듈로드
app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
})); 




var main = require('./router/main'); //라우터모듈 로드
var board = require('./router/board'); //게시판 라우터
var test = require('./router/test'); //라우터모듈 로드
var middle = require('./router/middle'); //라우터모듈 로드
var member = require('./router/member')(connection); //로그인 관련 라우터 

//app.use(sessionCheck); //미들웨어 전역 사용
app.use('/', main);
app.use('/board', sessionCheck , board);
app.use('/test', test);
app.use('/middle', middle);

app.use('/member', member);

module.exports = app;