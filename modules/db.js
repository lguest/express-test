
var mysql = require('mysql'); //mysql 모듈 로드 
var database = require('../config/database'); //데이터베이스 설정파일

var connection = mysql.createConnection(database);

connection.connect(function (err) {
    if (err) throw err;
});

module.exports = connection;