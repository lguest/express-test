var fs = require('fs');
var path = require('path');

var upfile = {
    createFolder:function(folder, createFolder){
        var tgFolder = path.join(folder,createFolder);
        console.log("createFolder ==> " + tgFolder);
        fs.mkdir(tgFolder, 0707, function(err){
            if(err){
                return false;
            }else{
                console.log('create newDir');	
                return true;
            }		
        });
    },
    searchFolder:function(folder, srhFolder){
        var rtnFolder;
        fs.readdir(folder, function (err, files) {
            if(err) throw err;
            files.forEach(function(file){
                if(file == srhFolder){
                    fs.stat(path.join(folder, file), function(err, stats){
                        console.log(stats);
                        if(stats.isDirectory()){
                            return path.join(folder, file);
                        }
                    });
                }
            });
        });
        return false;
    },
    arryCreateFolder:function(imgFolder, folderArr){
        var nFolder = imgFolder;
        for( folder in folderArr ){
            var status = searchFolder(nFolder, folderArr[folder]);
            if(!status){
                var createStatus = createFolder(nFolder, folderArr[folder]);
                nFolder = path.join(nFolder, folderArr[folder]);
            }
        }
    },
    file_write:function(folder, msg){
        fs.appendFile(folder,msg+"\r\n",function(err){
            if (err) throw err; 
            console.log('Data Add');
        });
        return false;
    }
}

module.exports = upfile;