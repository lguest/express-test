
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
/*
app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});
*/



io.on('connection', function(socket){
    socket.on('chat message', function(msg){ //chat message 이벤트 수신시
        io.emit('chat message', msg); //이벤트 발송
    });
    socket.on('disconnect', function(){ //종료시
        console.log('user disconnected');
    });
});

http.listen(3030, function(){
  console.log('listening on *:3030');
});