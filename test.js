var calc = function(param1, param2){
    this.num1 = param1;
    this.num2 = param2;

    this.plus = function(){
        
        return this.num1 + this.num2;
    }

    this.minus = function(){
        
        return this.num1 - this.num2;
    }
}


module.exports = calc;
