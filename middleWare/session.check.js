var common = require('../modules/common');

//세션체크 미들웨어
var sessionCheck = function (req, res, next) {
    console.log(req.session.user_id);
    if (req.session.user_id == undefined) {
        console.log(req.session.user_id);
        //res.redirect('/member/login');
        res.send(common.alert('로그인이 필요합니다. ', '/member/login'));
    } else {
        //console.log(req.session.user_id);
        next();
    }
};
module.exports = sessionCheck;