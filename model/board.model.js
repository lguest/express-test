var db = require('../modules/db');

var boardModel = {

    getBoardList:function(callback,  errorCallback){
        db.query('SELECT * FROM board ORDER BY board_idx DESC', function(err, result){
            if(err){
                errorCallback(err);
            }else{
                callback(result);
            }
        })
    },

    getBoardInfo: function(board_idx, callback, errorCallback){
        db.query('SELECT * FROM board WHERE board_idx = ? ', [ board_idx ], function (err, result) {
            if (err) {
                errorCallback(err);
            } else {
                callback(result[0]);
            }
        })
    },

    registBoard: function(data, callback, errorCallback){
        var sql = "INSERT INTO board (board_title, board_contents) value ( ? , ? )";

        db.query(sql , [data.board_title, data.board_contents], function(err, result){
            if (err) {
                errorCallback(err);
            } else {
                callback(result.insertId); //last insert id 리턴
            }
        });
    },

    modifyBoard: function (data, callback, errorCallback) {
        var sql = "UPDATE board SET board_title = ? , board_contents = ? WHERE board_idx = ?";

        db.query(sql, [data.board_title, data.board_contents, data.board_idx], function (err, result) {
            if (err) {
                errorCallback(err);
            } else {
                callback(result.affectedRows); //변경 건 수 
            }
        });
    }

}

module.exports = boardModel;