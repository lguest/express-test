var common = require('../modules/common');

var controller = function (data) {
    // we tell the client to execute 'new message'
    socket.broadcast.emit('new message', {
        username: socket.username,
        message: data
    });
}

module.exports = controller;