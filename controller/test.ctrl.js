var common = require('../modules/common');
var sqlite = require('../modules/sqlite');

/* api 로직 */
let users = [
    { id: 1, name: 'jinbro' },
    { id: 2, name: 'jinhyung' },
    { id: 3, name: 'park' }
];

let test = function (req, res) {

    var date = new Date();

    var start_date = common.getDateTime(date);

    res.render('test/page', {
        title: "test2",
        length: 5,
        start_date: start_date
    });
}

let test2 = function (req, res) {

    var date = new Date();

    var start_date = common.getDateTime(date);

    var list = [];
    sqlite.test(function (row) { //sql 라이트 실행
        
        list = row;

        res.render('test/page2', {
            title: "test2",
            length: 5,
            users: users,
            list: list
        });
        console.log(row);
        
    }); 

    
}


module.exports = {
    test: test,
    test2: test2,
};