var express = require('express'); //express 모듈로드
var common = require('../modules/common');
var boardModel = require('../model/board.model'); //board model 로드 

var board = {
    index:function(req, res){

        res.render('./board/index', {
            title: "MY HOMEPAGE",
            length: 5
        })

    },list:function(req, res){
        boardModel.getBoardList(function (result) {
            console.log(result);
            res.render('./board/list', {board_list:result})
        }, function (error) {
            res.send(common.goBack('DB 오류입니다. '));
        });

    }, write: function(req, res){
        var board_idx = req.query.board_idx; //get 파라미터 값

        if (board_idx == '' || board_idx == 'undefined') { // 입력폼
            res.send(common.goBack('게시물을 선택 하십시오. '));
        } else { //수정폼
            boardModel.getBoardInfo(board_idx, function (result) {
                console.log(result);
                res.render('./board/write', { board_info: result })
            }, function (error) {
                res.send(common.goBack('DB 오류입니다. '));
            });
        }
        
    }, proc_write:function(req, res){
        var data = req.body;

        if(data.board_title == '' || data.board_contents == ''){
            res.send(common.goBack('제목 및 내용을 입력 하십시오. '));
        }else{
            if(data.board_idx == ''){
                boardModel.registBoard(data, function(result){
                    res.send(common.alert('게시물이 등록되었습니다. ', '/board/list'));                
                }, function(error){
                    res.send(common.goBack('DB 오류입니다. '));
                });
            }else{
                boardModel.modifyBoard(data, function (result) {
                    res.send(common.alert('게시물이 수정되었습니다. ', '/board/list'));
                }, function (error) {
                    res.send(common.goBack('DB 오류입니다. '));
                });
            }
        }

    }, view: function(req, res){
        var board_idx = req.query.board_idx; //get 파라미터 값

        if (board_idx == '' || board_idx == 'undefined'){
            res.send(common.goBack('게시물을 선택 하십시오. '));
        }else{
            boardModel.getBoardInfo(board_idx, function (result) {
                console.log(result);
                res.render('./board/view', { board_info: result })
            }, function (error) {
                res.send(common.goBack('DB 오류입니다. '));
            }); 
        }
    }
}

module.exports = board;